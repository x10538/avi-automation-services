@PyR @Regresion
Feature: Consulta de PyR

  Background: Consulta de PyR con usuario enrolado
    Given el usuario inicia el flujo conversacional con Avi con numero de telefono "telefono"
    Then hacemos la consulta para saber el estado de mi reclamo

 # @ReclamoEnProceso
  #Scenario: [Reclamo en Proceso]
   # And ingresamos el número de reclamo "37582"
    #Then verificamos el codigo de respuesta "PERE_004" para el tipo de reclamo ingresado

 # @ReclamoAtendido
#  Scenario: [Reclamo Atendido con Éxito]
  #  And ingresamos el número de reclamo "89960"
   # Then verificamos el codigo de respuesta "PERE_002" para el tipo de reclamo ingresado

  @ReclamoNoEncontrado
  Scenario: [Reclamo no encontrado]
    And ingresamos el número de reclamo "89960"
    Then verificamos el codigo de respuesta "TRX_CLAIM_NOT_FOUND, MENU_003" para el tipo de reclamo ingresado

