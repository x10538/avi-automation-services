@ConsultaSaldoTC @Regresion
Feature:Consulta Saldo TC

  @ConsultaSaldoTC1
  Scenario: Se desea consultar el saldo de una Tarjeta de Crédito con Usuario Enrolado
    Given el usuario inicia el flujo de conversacional desde su "nrotelefono"
    When hace la consulta de Saldo de TC
    And se verificara si el usuario cuenta con una o más tarjetas
    And si cuenta con una tarjeta verificara su saldo sino escogera una tarjeta de la lista ""
    Then por ultimo salimos para poder cerrar el ticket

  @ConsultaSaldoTC2
  Scenario: Se desea consultar el saldo de una Tarjeta de Crédito con Usuario Enrolado
    Given el usuario inicia el flujo de conversacional desde su "nrotelefono"
    When hace la consulta de Saldo de TC
    And se verificara si el usuario cuenta con una o más tarjetas
    And si cuenta con una tarjeta verificara su saldo sino escogera una tarjeta de la lista "B"
    Then por ultimo salimos para poder cerrar el ticket

   # Examples:
    #  | Cantidad de Tarjetas | nrotelefono | lista |
     # | 1 tc                 | 51987659101 |       |
      #| Más de 1 tc          | 51966566088 | B     |