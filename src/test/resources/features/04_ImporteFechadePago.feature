@IMFP @Regresion
Feature:Importe Fecha de Pago de TC

  @IMFP_1
  Scenario: Se desea consultar el saldo de una Tarjeta de Crédito con Usuario Enrolado
    Given el usuario inicia el flujo de conversacional con su "nrotelefono"
    When hace la consulta sobre la fecha de pago de su TC
    And se verifica si el usuario cuenta con una o más tarjetas
    And si cuenta con una tarjeta verificara su fecha de pago sino escogera una tarjeta de la lista ""
    Then por ultimo salimos para cerrar el ticket

  @IMFP_2
  Scenario: Se desea consultar el saldo de una Tarjeta de Crédito con Usuario Enrolado
    Given el usuario inicia el flujo de conversacional con su "nrotelefono"
    When hace la consulta sobre la fecha de pago de su TC
    And se verifica si el usuario cuenta con una o más tarjetas
    And si cuenta con una tarjeta verificara su fecha de pago sino escogera una tarjeta de la lista "A"
    Then por ultimo salimos para cerrar el ticket

   # Examples:
   #   |             | nrotelefono | lista |
   #   | 1 tc        | 51987659101 |       |
   #   | Más de 1 tc | 51966566088 | A     |