package com.everis.avi.rest.steps;

import com.everis.avi.rest.fact.Access;
import com.everis.avi.rest.question.Mensaje;
import com.everis.avi.rest.task.Enrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.*;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.*;
/**
 * @author YorkCorreaLaRosa on 21/12/2020
 * @project avi-automation-services
 */

public class NuevoEnrolamientoStepDefinition {

    Util getConfigProperties = new Util();
    String Telefono;
    String codigoSesion = "";
    String match;

    @Given("el usuario hace el flujo de conversacion desde su {string}")
    public void elUsuarioHaceElFlujoDeConversacionDesdeSu(String nroTelefono) {
        theActorCalled("Test").has(Access.toAvi());
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Hola", codigoSesion, Telefono));
        codigoSesion = SerenityRest.lastResponse().path("sessionCode");
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @When("acepta los terminos y condiciones")
    public void aceptaLosTerminosYCondiciones() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("ACEPTO", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()
                        .body("answers.code", hasItem("CONE_004"))
                        .body("answers.code", hasItem("BVND_004")))
        );
    }

    @And("ingresa el tipo de documento {string} y numero de documento {string}")
    public void ingresaElTipoDeDocumentoYNumeroDeDocumento(String tipoDocumento, String numeroDocumento) {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento(getConfigProperties.getVariableSerenity("var_tipodoc") + " " + getConfigProperties.getVariableSerenity("var_dni"), codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @Then("confirma los datos ingresados anteriormente")
    public void confirmaLosDatosIngresadosAnteriormente() {
        match = SerenityRest.lastResponse().path("answers.code[0]").toString().replaceAll("\\[", "").replaceAll("]", "");

        if (match.equalsIgnoreCase("BVND_015")) {
            theActorInTheSpotlight().should(seeThatResponse(response ->
                    response.statusCode(HttpStatus.SC_OK).log().all()
                            .body("answers.code", hasItem("MENU_001"))
                            .body("answers.code", hasItem("MENU_002"))
            ));

        } else {
            theActorInTheSpotlight().should(seeThat("Codigo de Respuesta", Mensaje.codigoRespuesta(), equalTo("BVND_011")));
            theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Si", codigoSesion, Telefono));
            theActorInTheSpotlight().should(seeThatResponse(response ->
                    response.statusCode(HttpStatus.SC_OK).log().all()
                            .body("answers.code", hasItem("BVND_015"))
                            .body("answers.code", hasItem("BVND_014"))
                            .body("answers.code", hasItem("MENU_001"))
                            .body("answers.code", hasItem("MENU_002"))
            ));
        }
    }
}