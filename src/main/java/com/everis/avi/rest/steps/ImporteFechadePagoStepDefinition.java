package com.everis.avi.rest.steps;

import com.everis.avi.rest.fact.Access;
import com.everis.avi.rest.task.Enrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.hasItem;

/**
 * @author YorkCorreaLaRosa on 29/12/2020
 * @project avi-automation-services
 */

public class ImporteFechadePagoStepDefinition {

    Util getConfigProperties = new Util();
    private String cantidadTarjetas;
    String Telefono;
    String codigoSesion = "";

    @Given("el usuario inicia el flujo de conversacional con su {string}")
    public void elUsuarioIniciaElFlujoDeConversacionalConSu(String nroTelefono) {
        theActorCalled("Test").has(Access.toAvi());
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Hola", codigoSesion, Telefono));
        codigoSesion = SerenityRest.lastResponse().path("sessionCode");
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @When("hace la consulta sobre la fecha de pago de su TC")
    public void haceLaConsultaSobreLaFechaDePagoDeSuTC() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Quiero saber la fecha de pago de mi tc", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @And("se verifica si el usuario cuenta con una o más tarjetas")
    public void seVerificaSiElUsuarioCuentaConUnaOMásTarjetas() {
        cantidadTarjetas = SerenityRest.lastResponse().path("answers.code[0]").toString().replaceAll("\\[", "").replaceAll("]", "");
    }

    @And("si cuenta con una tarjeta verificara su fecha de pago sino escogera una tarjeta de la lista {string}")
    public void siCuentaConUnaTarjetaVerificaraSuFechaDePagoSinoEscogeraUnaTarjetaDeLaLista(String consultaTarjeta) {
        switch (cantidadTarjetas) {
            case "TRX_CARD_AMOUNT":
                theActorInTheSpotlight().should(seeThatResponse(response ->
                        response.statusCode(HttpStatus.SC_OK).log().all()
                               // .body("answers.code", hasItem("IMFP_016"))
                         .body("answers.code", hasItem("TRX_CARD_AMOUNT, CRRE_002"))
                       //  .body("answers.code", hasItem("IMFP_016"))
                ));
                break;

            case "TRX_CARD_LIST":
                theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento(consultaTarjeta, codigoSesion, Telefono));
                theActorInTheSpotlight().should(seeThatResponse(response ->
                        response.statusCode(HttpStatus.SC_OK).log().all()
                                .body("answers.code", hasItem("TRX_CARD_AMOUNT"))
                                .body("answers.code", hasItem("IMFP_017")))
                );
                break;
            default:
        }
    }

    @Then("por ultimo salimos para cerrar el ticket")
    public void porUltimoSalimosParaCerrarElTicket() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("salir", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()
                        .body("answers.code", hasItem("DESP_013"))
                 //       .body("answers.code", hasItem("DESP_012"))
                 //       .body("answers.title", hasItem("CLOSE_TICKET"))
                )
        );
    }
}